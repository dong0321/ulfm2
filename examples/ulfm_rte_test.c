/*
 * Copyright (c) 2018      The University of Tennessee
 *
 */

#include <stdio.h>
#include "mpi.h"

#include "../orte/mca/mca.h"
#include "../orte/mca/grpcomm/grpcomm.h"
#include "../opal/mca/base/mca_base_framework.h"
#include "../opal/mca/pmix/pmix.h"
#include "../ompi/mca/rte/rte.h"
#include "../ompi/mca/rte/orte/rte_orte.h"
#include "../ompi/include/ompi_config.h"


int rank;
double start, dfailure;
double mtff, Mtff, atff;

/* registration callback */
void errhandler_registration_callback(int status,
        size_t errhandler_ref,
        void *cbdata)
{
    printf("Handler register!\n");
}

void errhandler_callback(int status,
        const opal_process_name_t *source,
        opal_list_t *info, opal_list_t *results,
        opal_pmix_notification_complete_fn_t cbfunc,
        void *cbdata)
{
    opal_value_t *val;
    printf("Rank %d :event of status %d happens\n",rank, status);
    if (NULL != cbfunc) {
        cbfunc(OPAL_SUCCESS, NULL, NULL, NULL, cbdata);
    }
}


int main(int argc, char* argv[])
{
    int rc, size, len, st;
    pid_t pid;

    char version[MPI_MAX_LIBRARY_VERSION_STRING];
    opal_list_t ecode;
    opal_value_t *ekv;

    char name[255];
    int color;
    MPI_Comm fcomm, scomm;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    MPI_Comm_dup( MPI_COMM_WORLD, &fcomm );
    MPI_Comm_set_errhandler( fcomm, MPI_ERRORS_RETURN );
    if (rank ==1)
        color =1;
    else
        color =2;
    MPI_Get_library_version(version, &len);
    printf("Hello, world, I am %d of %d, (%s, %d)\n", rank, size, version, len);

    MPI_Get_library_version(version, &len);

    OBJ_CONSTRUCT(&ecode, opal_list_t);
    ekv = OBJ_NEW(opal_value_t);
    ekv->type = OPAL_INT;
    ekv->data.integer = -57;
    opal_list_append(&ecode, &ekv->super);
//    opal_pmix.register_evhandler(&ecode, NULL, errhandler_callback, errhandler_registration_callback, NULL);

    MPI_Barrier(MPI_COMM_WORLD);
    sleep(3);
    if (rank ==1)
    {
        pid = getpid();
        kill(pid, 9);
    }
    OPAL_LIST_DESTRUCT(&ecode);

    gethostname(name, 255);
    printf("%s PERF Rank orte %d : %f \n", name, rank, dfailure);

    MPI_Finalize();

    return 0;
}
